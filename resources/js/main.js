// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* ================================
                  ISOTOPE
=================================== */

$(document).ready(function() {
  $('.grid3').bind('touchstart', function(e) {

    $('.grid3').each().toggleClass('hover_effect');
  });

});

/*$(".mainnav div > ul").append('<li class="register"><a class="button red"href="">Register</a></li>');
*/

$(window).load(function() {


  $('.landing .slider-gallery .flexslider').flexslider({
    animation: "slide",
    itemWidth: 210,
    itemMargin: 5,
    controlNav: false,
    slideshow: false, 
    directionNav: true,
    prevText: "", 
    nextText: "",

  });

  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,
    directionNav: false,
    
  });
          $(".fancybox").fancybox();
  
});



/*$(document).ready(function(){
    var videos = [
    [{'type':'mp4', 'codecs':'avc1.42E01E, mp4a.40.2', 'src':'images/video/home3.mp4'},{'type':'webm', 'codecs':'vp8, vorbis', 'src':'images/video/home3.webm'}],
    [{'type':'mp4', 'codecs':'avc1.42E01E, mp4a.40.2', 'src':'images/video/home2.mp4'}, {'type':'webm', 'codecs':'vp8, vorbis', 'src':'images/video/home2.webm'}],  
    ];
    getRandomVideo(videos);
    // function for random and change src and type
    function getRandomVideo(videos) { 
        var number = Math.floor(Math.random()*videos.length);
        $('.media video source').eq(0).attr('type',videos[number][0].type).attr('src',videos[number][0].src);
        $('.media video source').eq(1).attr('type',videos[number][1].type).attr('src',videos[number][1].src);

    }


})*/








